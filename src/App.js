import './App.css';
import Header from './Components/Header/Header';
import Wrapper from './Containers/AppleWrapper/Wrapper';

function App() {
	return ( 
		<div className="App" >
			<Header/>
			<main>
				<Wrapper title="iPhone 13 Pro" subTitle="Oh. So. Pro." imgName="/images/iPhone13.png" bg="#FAFAFD" moreLink="#" buyUrl="#" />
				<Wrapper title="iPhone SE" subTitle="Love the power. Love the price." imgName="/images/iPhoneSE.png" bg="#FAFAFD" moreLink="#" buyUrl="#" />
			</main>
		</div>
	);
}

export default App;