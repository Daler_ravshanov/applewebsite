import React from 'react';
import SVGicons from '../SVGicons';
import './Header.css'

const links = [
    {
        title: "Store",
        url: ""
    },
    {
        title: "Mac",
        url: ""
    },
    {
        title: "iPad",
        url: ""
    },
    {
        title: "iPhone",
        url: ""
    },
    {
        title: "Watch",
        url: ""
    },
    {
        title: "AirPods",
        url: ""
    },
    {
        title: "TV & Home",
        url: ""
    },
    {
        title: "Only on Apple",
        url: ""
    },
    {
        title: "Accessories",
        url: ""
    },
    {
        title: "Support",
        url: ""
    },
]

const Header = () => {
    return (
        <header>
            <div className="links">
                <SVGicons icon="apple" />
                {links.map(({title, url}, index) => <a href={url} key={index} >{title}</a>)}
                <SVGicons icon="search" />
                <SVGicons icon="shop" />
            </div>
        </header>
    );
};

export default Header;