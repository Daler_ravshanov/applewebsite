import React from 'react';
import './Wrapper.css'

const Wrapper = ({title, subTitle, moreLink, buyUrl, imgName, bg}) => {
    const style = {
        backgroundColor: bg
    }

    return (
        <div className="wrapper"  style={style} >
            <div className="title">
                <h1>{title}</h1>
                <span>{subTitle}</span>
                <div className="row">
                    <a href={moreLink}>Learn more {">"} </a>
                    <a href={buyUrl}>Buy {">"} </a>
                </div>
            </div>
            <img className="bodyImage" src={imgName} alt="some iamge of apple product" />
        </div>
    );
};

export default Wrapper;